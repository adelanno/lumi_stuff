#!/nfshome0/lumipro/brilconda3/bin/python3

from __future__ import annotations
import logging
import pathlib
import socket

import pandas
import tables

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)

if socket.gethostname() == 'scx5-c2f06-36':
    PATH = pathlib.Path('/localdata/SSHFS.BRILDATA')
if socket.gethostname() in ('srv-s2d16-29-01','srv-s2d16-30-01'):
    PATH = pathlib.Path('/brildata')
if 'lxplus' in socket.gethostname():
    PATH = pathlib.Path('/eos/cms/store/group/dpg_bril/comm_bril/2022/online/')

def timer(func: typing.Callable) -> typing.Callable:
    '''decorator to time function execution'''
    import functools
    import timeit
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        t0 = timeit.default_timer()
        value = func(*args, **kwargs)
        t1 = timeit.default_timer()
        logging.debug(f'{func.__name__}({kwargs.get("file_path", "")}): {t1-t0}s')
        return value
    return wrapper

@timer
def read_hdf_node(file_path: pathlib.Path, node: str) -> tables.Table:
    logging.debug(file_path)
    return tables.open_file(file_path, 'r').root[node]

@timer
def hdf_to_awkward(file_path: pathlib.Path, node: str) -> awkward.Array:
    try:
        import awkward
    except ModuleNotFoundError:
        return
    with tables.open_file(file_path, 'r') as table:
        return awkward.Array(table.root[node][:])

@timer
def hdf_to_pandas(file_path: pathlib.Path, node: str) -> pandas.DataFrame:
    '''read open `node` in hdf file given by `file_path` and return as a `pandas.DataFrame`'''
    try:
        return pandas.read_hdf(file_path, key=node, mode='r')
    except KeyError as k:
        logging.error(k)
        return pandas.DataFrame()
    except ValueError as v:
        logging.warning(v)
        with tables.open_file(file_path, 'r') as table:
            return pandas.DataFrame(data=table.root[node][:].tolist(), columns=table.root[node].colnames)

@timer
def hdf_to_flat_pandas(file_path: pathlib.Path, node: str) -> pandas.DataFrame:
    with tables.open_file(file_path, 'r') as table:
        col = pandas.Series({col: descr.shape for col, descr in table.root[node].coldescrs.items()})
        data = pandas.DataFrame(table.root[node][:][col[col == ()].index])
        bx_data = [pandas.DataFrame(table.root[node][:][col]).add_prefix(f'{col}_') for col in col[col != ()].index]
    return pandas.concat([data, *bx_data], axis=1)

def read_fill(fill: int, node: str, year: int) -> pandas.DataFrame:
    return pandas.concat([hdf_to_pandas(file_path=f, node=node) for f in pathlib.Path(f'{PATH}/{str(year)[2:]}/{fill}/').glob('*.hd5')])

def write_hdf(file_path: pathlib.Path, node: str):
    pandas.Dataframe.to_hdf(file_path, key=node, mode='w', complevel=9, complib='blosc')
