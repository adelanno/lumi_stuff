#!/cvmfs/cms-bril.cern.ch/brilconda310/bin/python3

import functools
import logging
import pathlib
import sys
import timeit
import typing

import numpy
import pandas
import tables

# to do:
    # process `table.root.beam` first and remove it from `nodes`

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)

EOS_BRIL_DPG_PATH = pathlib.Path('/eos/cms/store/group/dpg_bril/comm_bril')
TMP_PATH = pathlib.Path(f'/eos/user/{pathlib.Path.home().parent.name}/{pathlib.Path.home().name}/tmp')
TMP_PATH.mkdir(exist_ok=True)

TCDS = ['fillnum', 'runnum', 'lsnum', 'nbnum']
ATTRS = {'algoid', 'bxmask', 'calibtag', 'channelid', 'cmson', 'datasource', 'datasourceid', 'fillscheme', 'firstlumibx', 'machinemode', 'maskhigh', 'masklow', 'maxlumibx', 'nbperls', 'ncollidable', 'ncollidingbx', 'norb', 'payloadtype', 'provider', 'publishnnb', 'targetegev','totsize'}
NODES = {'bcm1fagghist', 'bcm1flumi', 'bcm1futcalumi', 'beam', 'dtlumi', 'hfetlumi', 'hfoclumi', 'pltaggzero', 'pltlumizero', 'remuslumi', 'tcds'} # bcm1futca_agg_hist
# [node.name for node in table.root if ('lumi' in node.name) and (node.name not in ('bestlumi', 'dtlumi', 'pltslinklumi'))] + ['beam', 'pltaggzero', 'tcds']

def timer(func: typing.Callable) -> typing.Callable:
    '''Measure execution time of a function. Optionally, print passed args and kwargs.'''
    @functools.wraps(func)
    def wrapper(*args, **kwargs) -> typing.Any:
        t0 = timeit.default_timer()
        value = func(*args, **kwargs)
        t1 = timeit.default_timer()
        logging.debug(f'{func.__module__}.{func.__name__}: {t1-t0}s')
        return value
    return wrapper

@timer
def n_collidable(node: tables.Table, idx: numpy.array, median_thr: float = 0.01):
    logging.debug('calculating number of colliding bunches')
    beam = node[idx][['bxintensity1', 'bxintensity2', 'ncollidable']]
    ncollidable = ((beam['bxintensity1'] >= median_thr * numpy.median(beam['bxintensity1'])) * (beam['bxintensity2'] >= median_thr * numpy.median(beam['bxintensity2']))).sum(axis=1)
    if any(ncollidable != beam['ncollidable']):
        logging.error(f"'ncollidable' (beam[bxintensity1]*beam[bxintensity2]) != beam['ncollidable']")
    if (numpy.unique(beam['ncollidable']).shape[0] != 1):
        logging.error(f"'ncollidable' is not unique: {list(numpy.unique(beam['ncollidable'])) = }")
    return ncollidable

@timer
def get_scalar_columns_and_attrs(node: tables.Table, idx: numpy.array = slice(None)) -> tuple[set[str], dict[str, typing.Any]]:
    logging.debug('checking for columns with constant values')
    scalar_columns = {col for col, dtype in node.coldtypes.items() if not dtype.shape}
    attr_data = node[idx][list(set(node.colnames) & ATTRS)]
    attrs = {}
    for key in attr_data.dtype.names:
        values, counts = numpy.unique(attr_data[key], return_counts=True)
        if (values.size == 1) or ((values.size == 2) and (counts.min() == 1)):
            val = values[counts.argmax()]
            attrs[key] = val if (not isinstance(val, bytes)) else val.decode('utf-8')
    _ = [logging.warning(f'{col} is non-uniform') for col in (scalar_columns - attrs.keys() & ATTRS)] 
    return (scalar_columns, attrs)

@timer
def read_scalar_columns(node: tables.Table, idx: numpy.array = slice(None)) -> pandas.DataFrame:
    logging.debug('reading non-array columns')
    scalar_columns, attrs = get_scalar_columns_and_attrs(node=node, idx=idx)
    df = pandas.DataFrame(node[idx][list(scalar_columns - attrs.keys())])
    df[f'date_time'] = pandas.to_datetime((df.pop('timestampsec') + df.pop('timestampmsec')/1000), unit='s', utc=True)
    prefix_node_name = {col: f'{node.name}_{col}' for col in (set(df.columns) - set(TCDS))}
    df = df.rename(columns=prefix_node_name)
    df.attrs = dict(node_name=node.name) | {f'{node.name}_{key}': val for key, val in attrs.items()}
    df[df.select_dtypes(object).columns] = df.select_dtypes(object).apply(lambda row: row.str.decode('utf-8')) # df[df.select_dtypes('number').columns] = df.select_dtypes('number').astype('uint32')
    return df

@timer
def pivot_per_channel(df: pandas.DataFrame) -> pandas.DataFrame:
    logging.debug('pivoting per-channel data')
    node_name = df.attrs.get('node_name') # {attr.split('_')[0] for attr in df.attrs.keys()}.pop()
    df_pivot = df.pivot(index=TCDS, values=[f'{node_name}_data', f'{node_name}_date_time'], columns=[f'{node_name}_channelid']).reset_index() # df_pivot = df.groupby(TCDS + [f'{node_name}_channelid']).first().unstack(level=f'{node_name}_channelid').reset_index()
    df_pivot.columns = [f'{col[0]}_{col[1]:02d}' if (col[1] != '') else col[0] for col in df_pivot.columns]
    df_pivot[df_pivot.select_dtypes(object).columns] = df_pivot.select_dtypes(object).astype(float)
    df_pivot[f'{node_name}_date_time'] = pandas.concat([df_pivot.pop(col) for col in df_pivot.select_dtypes('datetime64[ns, UTC]').columns], axis=1).apply(func=lambda row: pandas.DatetimeIndex(row).mean(), axis=1)
    df_pivot.attrs = df.attrs
    return df_pivot

@timer
def filter_stable_beams(fill: int, beam: tables.Table) -> numpy.ndarray:
    logging.debug(f"filtering 'STABLE BEAMS' during fill {fill}")
    return beam[beam.get_where_list(f'(fillnum == {fill}) & (status == "STABLE BEAMS")')][TCDS]

@timer
def filter_tcds(tcds: numpy.ndarray, node: tables.Table) -> numpy.array:
    fill, run, ls = numpy.unique(tcds['fillnum']), numpy.unique(tcds['runnum']), numpy.unique(tcds['lsnum'])
    if (len(fill) != 1) or (len(run) != 1):
        raise ValueError('multiple `fill` or `run` data!')
    logging.debug(f'filtering (fillnum == {fill[0]}) & (runnum == {run[0]}) & (lsnum >= {ls.min()}) & (lsnum <= {ls.max()})')
    return node.get_where_list(f"(fillnum == {fill[0]}) & (runnum == {run[0]}) & (lsnum >= {ls.min()}) & (lsnum <= {ls.max()})")

def sigma_vis_from_avg_avgraw():
    pass

@timer
def orbit_integrate_lumi(node: tables.Table, idx: numpy.array, k_plt: float = 11245/300) -> pandas.DataFrame:
    logging.debug('summing per-bcid data')
    if node.name == 'pltaggzero':
        return pandas.Series(name=f'{node.name}_data', data=(k_plt * -numpy.log(node[idx]['data']/2**14)).sum(axis=1))
    per_bcid_lumi = ({'agghist', 'bx', 'bxraw', 'data'} & set(node.colnames))
    return pandas.DataFrame({f'{node.name}_{col}': node[idx][col].sum(axis=1) for col in per_bcid_lumi}).rename(columns={f'{node.name}_agghist': f'{node.name}_data'})

@timer
def orbit_integrate_lumi_iter(node: tables.Table, idx: numpy.array, k_plt: float = 11245/300) -> pandas.DataFrame:
    logging.debug('summing per-bcid data using iterrows')
    if node.name == 'pltaggzero':
        return pandas.Series(name=f'{node.name}_data', data=[k_plt * -numpy.log(row['data']/2**14).sum() for nrow, row in enumerate(node.iterrows()) if (nrow in idx)])
    per_bcid_lumi = ({'agghist', 'bx', 'bxraw', 'data'} & set(node.colnames))
    return pandas.DataFrame({f'{node.name}_{col}': [row[col].sum() for nrow, row in enumerate(node.iterrows()) if (nrow in idx)] for col in per_bcid_lumi}).rename(columns={f'{node.name}_agghist': f'{node.name}_data'})

def orbit_integrate_beam(node: tables.Table, idx: numpy.array):
    per_bcid_beam = {'bxconfig1', 'bxconfig2', 'bxintensity1', 'bxintensity2', 'collidable'}

@timer
def read_node(node: tables.Table, idx: numpy.array, hd5_filename: str) -> pandas.DataFrame:
    logging.info(f'reading {node.name = }')
    df_scalar = read_scalar_columns(node=node, idx=idx)
    df_array = orbit_integrate_lumi_iter(node=node, idx=idx) # orbit_integrate_lumi(node=node, idx=idx)
    df = pandas.concat([df_scalar, df_array], axis=1)
    df.attrs = df_scalar.attrs
    if node.name in ('bcm1fagghist', 'bcm1futca_agg_hist', 'pltaggzero'):
        df = pivot_per_channel(df=df)
    if node.name == 'beam':
        df[f'{node.name}_ncollidable_bxintensity'] = n_collidable(node=node, idx=idx)
    df.to_pickle(f"{TMP_PATH}/{hd5_filename}_{df.attrs['node_name']}.pkl")
    return df

@timer
def process_hd5(hd5_path: pathlib.Path, fill: int) -> pandas.DataFrame:
    logging.info(f'{hd5_path=} ({hd5_path.stat().st_size / 10**9} GB)')
    lumi_dict = dict()
    with tables.open_file(filename=hd5_path, mode='r') as table:
        tcds = filter_stable_beams(fill=fill, beam=table.root.beam)
        if tcds.size == 0:
            logging.warning("'STABLE BEAMS' not declared!")
            return pandas.DataFrame()
        for node in (NODES & {node.name for node in table.root}):
            logging.debug(f"processing node '{node}'")
            idx = filter_tcds(tcds=tcds, node=table.root[node])
            lumi_dict[node] = read_node(node=table.root[node], idx=idx, hd5_filename=hd5_path.stem)
    if not lumi_dict.values():
        return pandas.DataFrame()
    lumi = pandas.concat([df.set_index(TCDS) for df in lumi_dict.values()], axis=1)
    lumi.attrs = {k: v for df in lumi_dict.values() for k, v in df.attrs.items()}
    lumi['date_time'] = pandas.concat([lumi.pop(col) for col in lumi.select_dtypes('datetime64[ns, UTC]').columns], axis=1).apply(func=lambda row: pandas.DatetimeIndex(row).mean(), axis=1)
    lumi = lumi.reset_index()
    lumi.to_pickle(f'{TMP_PATH}/{hd5_path.stem}.pkl')
    return lumi

@timer
def process_fill(hd5_files: list[pathlib.Path], fill: int) -> pandas.DataFrame:
    logging.info(f'{fill = }')
    if not hd5_files:
        logging.error('no hd5_files provided!')
        return pandas.DataFrame()
    # max_size = max(file.stat().st_size for file in hd5_files)
    # if max_size > 30E9:
    #     logging.warning(f'skipping fill {fill} since data exceeds 30 GB ({max_size})')
    #     return pandas.DataFrame()
    lumi = [process_hd5(hd5_path=hd5_path, fill=fill) for hd5_path in hd5_files]
    LUMI = pandas.concat(lumi, axis=0).reset_index(drop=True)
    LUMI.attrs = {key: val for df in lumi for key, val in df.attrs.items()}
    return LUMI

def check_attrs(lumi: pandas.DataFrame) -> dict[str, typing.Any]:
    attrs = {}
    [logging.warning(f'{key}: {val} != {attrs.get(key)}') for df in lumi for key, val in df.attrs.items() if attrs.get(key) and (val != attrs[key])]
    # for df in lumi:
    #     for key, val in df.attrs.items():
    #         if attrs.get(key) and (val != attrs[key]):
    #             logging.warning(f'{key}: {val} != {attrs.get(key)}')

def get_fills(year: int = 2023, fill_type: str = 'physics', min_date: str = '2023-06-17', max_date: str = '2023-12-31', min_duration: str = '0 hour', max_duration: str = '24 hour'):
    fills = pandas.json_normalize(pandas.read_json(f'https://lpc.web.cern.ch/cgi-bin/fillTableReader.py?action=load&year={year}').data)
    date_filter = (pandas.to_datetime(fills.start_sb) > min_date) & (pandas.to_datetime(fills.start_sb) < max_date)
    duration_filter = (pandas.to_timedelta(fills.length_sb) >= min_duration) & (pandas.to_timedelta(fills.length_sb) <= max_duration)
    return fills[(fills.type == fill_type) & date_filter & duration_filter].fillno.astype(int).to_list()

def MAIN(year: int = 2023):
    fills = get_fills(year=year, fill_type='physics', max_duration='24 hour')
    for fill in fills:
        file_path = pathlib.Path(f'{EOS_BRIL_DPG_PATH}/{year}/physics/{fill}.pkl')
        if file_path.exists():
            logging.warning(f"skipping fill {fill} since '{file_path}' already exists")
            continue
        hd5_files = sorted((EOS_BRIL_DPG_PATH/f'{year}/physics/').glob(f'{fill}/*hd5'))
        data = process_fill(hd5_files=hd5_files, fill=fill)
        if not data.empty:
            logging.info(f"writing to '{file_path}'")
            data.to_pickle(file_path)

def main(fill: int, year: int = 2023):
    hd5_files = sorted((EOS_BRIL_DPG_PATH/f'{year}/physics/').glob(f'{fill}/*hd5'))
    data = process_fill(hd5_files=hd5_files, fill=fill)
    if not data.empty:
        logging.info(f"writing to '{fill}.pkl'")
        data.to_pickle(pathlib.Path(f'{fill}.pkl'))

if __name__ == '__main__':
    MAIN()
    # main(fill=sys.argv[1])

def test():
    year=2023; fill=9066; hd5_path = sorted((EOS_BRIL_DPG_PATH/f'{year}/physics/').glob(f'{fill}/*hd5'))[2];
    table = tables.open_file(filename=hd5_path, mode='r')
    node = table.root.hfetlumi; idx = filter_stable_beams(fill=fill, beam=table.root.beam, node=node)
    return fill, table, node, idx
    lumi = process_hd5(hd5_path=hd5_path, fill=fill)
    return pandas.concat([df.set_index(TCDS) for df in lumi.values()], axis=1).reset_index()


class Trash:

    def static_columns(table: tables.File):
        static_col = {node.name: [col for col in node.colnames if (col not in TCDS) and (numpy.unique(node[:][col]).shape == (1,))] for node in table.root}
        return sorted({col for nodes in static_col.values() for col in nodes})

    def columns(table: tables.File):
        all_columns =  {col for node in table.root for col in node.colnames}
        per_bx_columns = [{node.name: per_bx_col} for node in table.root if (per_bx_col := {col for col, dtype in node.coldtypes.items() if dtype.shape})]
        scalar_columns = [{node.name: per_bx_col} for node in table.root if (per_bx_col := {col for col, dtype in node.coldtypes.items() if not dtype.shape})]
        for node in table.root:
            pandas.DataFrame(node[:][[col for col, dtype in node.coldtypes.items() if not dtype.shape]]).apply(lambda col: col.unique().size)

    def string_columns(node: tables.Table):
        for col, dtype in node.coldtypes.items():
            if dtype.char == 'S':
                node[:][col] = numpy.char.decode(node[:][col], encoding='utf-8')
        return node
        for col, descr in node.coldescrs.items():
            if descr.dflt == b'':
                descr.dflt = ''

    def columns_to_attr(node: tables.Table):
        attr = dict()
        for key in (scalar_columns & ATTRS):
            val = numpy.unique(node[:][key])
            if len(val) == 1:
                attr[key] = val[0]
        return attr

    def columns_to_attr(node: tables.Table) -> tuple[set[str], dict[str, typing.Any]]:
        scalar_columns = {col for col, dtype in node.coldtypes.items() if not dtype.shape}
        attrs = {key: val[0] for key in ATTRS if (key in node.colnames) and (len(val := numpy.unique(node[:][key])) == 1)}
        # tcds = set(TCDS + ['timestampsec', 'timestampmsec']) if node.name == 'tcds' else set(TCDS)
        # attrs = {key: val[0] for key in (scalar_columns - tcds) if (len(val := numpy.unique(node[:][key])) == 1)}
        return scalar_columns, attrs

    def collidable(table: tables.File):
        bx_intensity = table['beam'][:]['bxintensity1'] + table['beam'][:]['bxintensity2']
        bx_colliding = bx_intensity.astype(bool).sum(axis=0)
        assert numpy.unique(bx_colliding[bx_colliding != 0]).size == 1
        assert numpy.unique(bx_colliding[bx_colliding != 0])[0] == table['beam'][:].size
        return bx_colliding.astype(bool)

    def plt_zero_counts_to_lumi(bx_data: numpy.ndarray, k: float = 11245/300) -> numpy.array:
        return k * -numpy.log(bx_data/2**14)

    def orbit_integrate(node: tables.Table) -> numpy.array:
        if node.name == 'pltaggzero':
            return plt_zero_counts_to_lumi(bx_data=node[:]['data']).sum(axis=1)
        if 'bx' in node.colnames:
            return node[:]['bx'].sum(axis=1)
        if ('bxintensity1' in node.colnames) and ('bxintensity2' in node.colnames):
            return collidable(node)

    def read_scalar_columns(node: tables.Table, columns: list[str] = None) -> pandas.DataFrame:
        scalar_col = list(set(node.colnames) & set(columns)) if columns else [col for col, dtype in node.coldtypes.items() if not dtype.shape]
        return pandas.DataFrame(node[:][scalar_col])

    def read_node(node: tables.Table, columns: list[str] = None) -> pandas.DataFrame:
        df = pandas.concat([read_scalar_columns(node, columns), pandas.Series(data=orbit_integrate(node), name=node.name)], axis=1)
        df = df[df[node.name] >= 0]
        df = df[(df.timestampsec != 0)]
        df['date_time'] = pandas.to_datetime((df.pop('timestampsec') + df.pop('timestampmsec')/1000), unit='s', utc=True)
        if ('avg' in df) and ('avgraw' in df):
            df[f'{node.name}_avg'], df[f'{node.name}_avgraw'] = df.pop('avg'), df.pop('avgraw')
        if (df.channelid == 0).all():
            _ = df.pop('channelid')
        else:
            df = pivot_per_channel(df=df)
        return df.sort_values('date_time')

    def merge_asof(left: pandas.DataFrame, right: pandas.DataFrame):
        common_columns = ['fillnum', 'runnum', 'lsnum', 'nbnum']
        right = right.astype(dict(left[common_columns].dtypes))
        return pandas.merge_asof(left=left, right=right, on='date_time', by=common_columns, direction='nearest')

    def process_hd5(hd5_path: pathlib.Path, nodes: list[str], columns: list[str]) -> pandas.DataFrame:
        logging.info(f'{hd5_path=}')
        with tables.open_file(filename=hd5_path, mode='r'):
            table = tables.open_file(filename=hd5_path, mode='r')
        lumi = {node: read_node(table.root[node], columns=columns) for node in nodes if (node in table.root)}
        lumi = dict(sorted(lumi.items(), key=lambda node: node[1].shape[0], reverse=False))
        data = lumi.popitem()[1] # Remove and return a (key, value) pair from the dictionary. Pairs are returned in LIFO order.
        for df in lumi.values():
            data = merge_asof(data, df)
        return data

    def process_fill(year: int = 2023, fill: int = 8866) -> pandas.DataFrame:
        logging.info(f'{year=} {fill=}')
        nodes = ['bcm1flumi', 'bcm1futcalumi', 'beam', 'hfetlumi', 'hfoclumi', 'pltaggzero'] # 'beam', 'tcds', 'bestlumi', 'bcm1fagghist', 'bcm1futca_agg_hist']
        # tcds_col = ['fillnum', 'runnum', 'lsnum', 'nbnum', 'timestampsec', 'timestampmsec', 'cmson']
        # lumi_col = ['channelid', 'avg', 'avgraw']
        # beam_col = ['status', 'egev', 'intensity1', 'intensity2', 'machinemode', 'fillscheme', 'ncollidable']
        # columns = [*tcds_col, *lumi_col, *beam_col] # columns = ['fillnum', 'runnum', 'lsnum', 'nbnum', 'timestampsec', 'timestampmsec'] + ['channelid', 'avg', 'avgraw'] + ['status', 'egev', 'intensity1', 'intensity2', 'machinemode', 'fillscheme', 'ncollidable']
        columns = ['fillnum', 'runnum', 'lsnum', 'nbnum', 'timestampsec', 'timestampmsec', 'channelid', 'avg', 'avgraw']
        hd5_files = sorted((EOS_BRIL_DPG_PATH/f'{year}/physics/').glob(f'{fill}/*hd5'))
        if not hd5_files:
            return pandas.DataFrame()
        dir_size = sum(file.stat().st_size for file in hd5_files)
        if dir_size > 20E9:
            logging.warning(f'skipping fill {fill} since data exceeds 20 GB ({dir_size})')
            return pandas.DataFrame()
        return pandas.concat([process_hd5(hd5_path, nodes, columns) for hd5_path in sorted((EOS_BRIL_DPG_PATH/f'{year}/physics/').glob(f'{fill}/*hd5'))]).reset_index(drop=True)

    def awkward_test():
        import awkward, pathlib, tables
        table = tables.open_file(filename=pathlib.Path(f'{EOS_BRIL_DPG_PATH}/2024/physics/9700/9700_381478_2406021350_2406021358.hd5'), mode='r')
        string_cols = {col for col, dtype in node.coldtypes.items() if (dtype.char == 'S') for node in table.root}
        data = awkward.Array([node[:][[col for col in node.colnames if col not in string_cols]] for node in table.root])
        node = table.root.hfoclumi
        # awkward.Array(node[:]['calibtag'].astype(str))
        hfoclumi = awkward.Array(node[:][[col for col in node.colnames if col != 'calibtag']])
        hfoclumi = hfoclumi[hfoclumi.timestampsec != 0]
        hfoclumi[node.name] = awkward.sum(hfoclumi.bx, axis=1)
