#!/usr/bin/env python3

import functools
import logging
import json
import pathlib
import sys
import timeit
import typing

import numpy
import pandas
import rich.logging
import tables

'''
Script to create "sidecar" files for BRIL hd5 physics files indicating, for each node, the start and end rows during which stable beams were declared (based on the `beam` node).
This makes it simple to only read hd5 files that contain stable beams, and only read each node's row numbers corresponding to stable beams (which is generally different for each node).
'''

logging.basicConfig(level=logging.DEBUG, format='%(message)s', handlers=[rich.logging.RichHandler(rich_tracebacks=True, log_time_format='[%Y-%m-%d %H:%M:%S]')])

EOS_BRIL_DPG = pathlib.Path('/eos/cms/store/group/dpg_bril/comm_bril')

def timer(func: typing.Callable) -> typing.Callable:
    '''Dectorator to log the execution time of a function.'''
    @functools.wraps(func)
    def wrapper(*args, **kwargs) -> typing.Any:
        t0 = timeit.default_timer()
        value = func(*args, **kwargs)
        t1 = timeit.default_timer()
        logging.debug(f'{func.__name__}: {t1-t0}s')
        return value
    return wrapper

@timer
def identify_stable_beams(beam: tables.Table) -> pandas.DataFrame:
    '''Return TCDS info (fill, run, ls, nb) during which stable beams were declared and verify that the data corresponds to exactly one fill and one run.'''
    logging.debug(f"identifying 'LS' range inside 'STABLE BEAMS'")
    tcds = pandas.DataFrame({field: row[field] for field in ('fillnum', 'runnum', 'lsnum', 'nbnum')} for row in beam.where("status == 'STABLE BEAMS'")) # https://www.pytables.org/usersguide/libref/structured_storage.html#tables.Table.where
    if tcds.empty:
        return pandas.DataFrame()
    if (tcds.fillnum.unique().size > 1):
        raise ValueError(f"stable beam data includes multiple fills: {tcds.fillnum.unique().tolist()}")
    if (tcds.runnum.unique().size > 1):
        raise ValueError(f"stable beam data includes multiple runs: {tcds.runnum.unique().tolist()}")
    return tcds

@timer
def filter_ls(ls: numpy.ndarray, node: tables.Table) -> tuple[int, int]:
    '''Return the start and end row numbers in `node` corresponding to the first and last LS in stable beams.'''
    logging.debug(f"filtering node '{node.name}': (lsnum >= {ls.min()}) & (lsnum <= {ls.max()})")
    ls_range = node.get_where_list(f"(lsnum == {ls.min()}) | (lsnum == {ls.max()})") # https://www.pytables.org/usersguide/libref/structured_storage.html#tables.Table.get_where_list
    return (ls_range.min().item(), ls_range.max().item()) if ls_range.size else (0, 0)

@timer
def stable_beams(hd5_path: pathlib.Path):
    '''If the 'beam' node exists, identify TCDS data inside stable beams; then determine the start/end rows corresponding to the first/last LS inside stable beams, and write as a JSON file.'''
    logging.info(f'{hd5_path=} ({hd5_path.stat().st_size / 10**9} GB)')
    stable_beams = dict()
    with tables.open_file(filename=hd5_path, mode='r') as table:
        if not ('beam' in table.root):
            return logging.error('no `beam` node available!')
        tcds = identify_stable_beams(beam=table.root.beam)
        if tcds.empty:
            return logging.warning("'STABLE BEAMS' not declared!")
        stable_beams['TCDS'] = tcds.to_numpy().tolist()
        for node in table.root:
            stable_beams[node.name] = filter_ls(ls=tcds.lsnum.unique(), node=node)
    with (hd5_path.parent/f'{hd5_path.stem}_stable_beams.json').open(mode='w') as fp:
        json.dump(obj=stable_beams, fp=fp)

def main(year: int = 2024):
    for hd5_path in pathlib.Path(f'{EOS_BRIL_DPG}/{year}/physics/').glob('*/*.hd5'):
        if (hd5_path.parent/f'{hd5_path.stem}_stable_beams.json').exists():
            logging.warning(f'"{hd5_path.stem}_stable_beams.json" already exists. skipping.')
            continue
        stable_beams(hd5_path=hd5_path)

if __name__ == '__main__':
    main()
