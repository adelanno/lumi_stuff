#!/nfshome0/lumipro/brilconda3/bin/python3

import pathlib
import typing

import numpy
import pandas
import tables

import hdf

def k(sigma_vis: float) -> float:
    LHC_FREQ = 11245.613 # ~ SPEED_OF_LIGHT/LHC_CIRCUMFERENCE ~ 299792458/26658.87 [https://s3.cern.ch/inspire-prod-files-3/37843e7885386ec5cf479f7c430ed3df]
    return LHC_FREQ/sigma_vis

def plt_mu(bx_data: numpy.ndarray, k: float) -> numpy.ndarray:
    '''Calculates pileup based on `k`. Note that `bx_data` values correspond to a bunch crossing with zero hits: 4*nibble == 4*(2**12) == 16384'''
    bx_data = numpy.stack(bx_data) if isinstance(bx_data, pandas.Series) else bx_data # [how to convert a Series of arrays into a single matrix in pandas/numpy?](https://stackoverflow.com/a/48793939/13019084)
    bx_data[bx_data > 2**14] = 2**14 # set values > 2**14 to zero hits (this usually only happens at the beginning of a file)
    return k * -numpy.log(bx_data/2**14)
    # return k * -numpy.log(bx_data / bx_data.max(axis=1)[:,None]) # [Numpy: Divide each row by a vector element](https://stackoverflow.com/a/19602209/13019084):

def colliding_mask(file_path: pathlib.Path) -> pandas.Series:
    '''determine colliding BCIDs from `beam.collidable` arrays'''
    beam = hdf.hdf_to_pandas(file_path=file_path, node='beam')
    return pandas.Series(numpy.stack(beam.collidable).max(axis=0))

def leading_mask(colliding_bx: pandas.Series) -> numpy.ndarray:
    '''determine leading bunches'''
    return (colliding_bx.diff() == 1).astype(int)

def train_mask(colliding_bx: pandas.Series) -> numpy.ndarray:
    '''determine train bunches'''
    return (colliding_bx - leading_mask(colliding_bx))

def bx_mask(file_path: pathlib.Path) -> pandas.DataFrame:
    '''store colliding BCIDs'''
    colliding_bx = colliding_mask(file_path)
    return pandas.DataFrame({'colliding': colliding_bx, 'leading': leading_mask(colliding_bx), 'train': train_mask(colliding_bx)})

def chMask(disabled_ch: list[int]) -> tuple[int]:
    '''create channel masks based on `disabled_ch` list'''
    mask = (0x1, 0x2, 0x10, 0x20, 0x100, 0x200, 0x1000, 0x2000)
    low = dict(zip(range(0, 7), mask))
    high = dict(zip(range(8, 16), mask))
    mask_low = 0x3333 - sum(low.get(ch, 0) for ch in disabled_ch)
    mask_high = 0x3333 - sum(high.get(ch, 0) for ch in disabled_ch)
    return (mask_low, mask_high)

def plt_reprocess(file_path: pathlib.Path, active_ch: typing.List[int] = [10,11,12,13,14,15]):
    pltaggzero = hdf.hdf_to_pandas(file_path=file_path, node='pltaggzero')
    if pltaggzero.empty:
        return
    pltaggzero = pltaggzero[pltaggzero.channelid.isin(active_ch)]
    pltaggzero['mu'] = plt_mu(bx_data=pltaggzero.data, k=k(sigma_vis=300)).tolist()
    return pltaggzero
    bxmask = bx_mask(file_path)

################################################################################

file_path = pathlib.Path('/eos/cms/store/group/dpg_bril/comm_bril/2022/online/8030/8030_356043_2207221757_2207221832.hd5') # pathlib.Path('/localdata/SSHFS.BRILDATA/22/8111/8111_357070_2208090043_2208090101.hd5')
pltaggzero = plt_reprocess(file_path)
bxmask = bx_mask(file_path)

def orbit_integrate(bx_data: numpy.ndarray) -> pandas.Series:
    # bx_data = numpy.stack(bx_data) if isinstance(bx_data, pandas.Series) else bx_data
    # if bx_data.shape[1] != 3564:
    #     raise ValueError('`bx_data` must be a 2-D `numpy.ndarray` with 3564 BCID "columns"')
    return pandas.Series(bx_data.sum(axis=1))

def plt_ch_ratio(fill: int, ch: int) -> pandas.Series:
    data = hdf.read_fill(fill=fill, node='pltaggzero', year=2022)
    data['mu'] = plt_mu(bx_data=numpy.stack(data.data), k=11245/300).sum(axis=1)
    chData = data[data.channelid == ch]['mu'].reset_index(drop=True)
    otherCh = data[data.channelid != ch].groupby(['runnum','lsnum', 'nbnum'])['mu'].median().reset_index(drop=True)
    return (chData/otherCh).replace(numpy.inf, numpy.nan).dropna()
