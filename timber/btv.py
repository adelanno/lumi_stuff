#!/home/acc-py/venvs/pytimber/bin/python

from __future__ import annotations
import logging

import pandas
import pytimber
import matplotlib.pyplot

import query

def beam_dump_ts(fill_number: int = 9639) -> tuple[float, float]:
    t1, t2 = query.fill_timestamps(fill_number=fill_number, beam_modes=['ADJUST', 'STABLE', 'BEAMDUMP', 'RAMPDOWN'])
    return query.query(pattern_or_list=['HX:DUMPED1'], t1=t1, t2=t2)['HX:DUMPED1'][0][0]

def plot_btv(btv: numpy.array, title: str):
    matplotlib.pyplot.imshow(btv, cmap='hot', interpolation='nearest')
    matplotlib.pyplot.title(title)
    matplotlib.pyplot.savefig(f'plots/{title}.png', dpi=300)

def btv(fill_number: int = 9639) -> tuple[numpy.array, numpy.array]:
    ts = beam_dump_ts(fill_number=fill_number)
    response = query.query(pattern_or_list=['TD68.BTVDD.689339.B1/Image#imageSet', 'TD62.BTVDD.629339.B2/Image#imageSet'], t1=ts-60, t2=ts+60)
    [b1_ts], [b1] = response['TD68.BTVDD.689339.B1/Image#imageSet']
    [b2_ts], [b2] = response['TD62.BTVDD.629339.B2/Image#imageSet']
    # assert (ts == max(b1_ts, b2_ts))
    b1, b2 = b1.reshape([280,320]), b2.reshape([280,320])
    ts = pandas.to_datetime(ts, unit='s', utc=True).strftime('%Y%m%d-%H%M%S')
    plot_btv(btv=b1, title=f"{ts}-B1")
    plot_btv(btv=b2, title=f"{ts}-B2")
    return (b1, b2)
