#!/home/acc-py/venvs/pytimber/bin/python

import pathlib

import pandas
import plotly.express
import plotly.graph_objects


def test():
    return query.test()

def lumi_data(fill_number: int, n_colliding: int = 2340):
    import query
    import lumi_ratio
    t1, t2 = query.fill_timestamps(fill_number, ['ADJUST', 'STABLE'])
    data = lumi_ratio.query_variables(t1, t2)
    data['cms_lumi/atlas_lumi'] = data['cms_lumi (Hz/ub)'] / data['atlas_lumi (Hz/ub)']
    data['SBIL'] = data['cms_lumi (Hz/ub)'] / n_colliding
    data.attrs = dict(fill_number=fill_number)
    return data

def ratio(data: pandas.DataFrame):
    fig = plotly.graph_objects.Figure()
    colorway = fig.to_dict()['layout']['template']['layout']['colorway']
    marker = dict(color=data.SBIL, colorscale='Viridis', size=8, line_width=0.2, colorbar=dict(title='SBIL', orientation='h'))
    customdata = data[['cms_lumi/atlas_lumi', 'atlas_lumi (Hz/ub)', 'cms_lumi (Hz/ub)', 'SBIL']]
    hovertemplate = 'CMS lumi / ATLAS lumi: %{customdata[0]}<br>' + 'ATLAS lumi (Hz/ub): %{customdata[1]}<br>' + 'CMS lumi (Hz/ub): %{customdata[2]}<br>' + 'CMS SBIL (Hz/ub): %{customdata[3]}<br>'
    fig.add_trace(plotly.graph_objects.Scattergl(x=data.UTC, y=data['cms_lumi/atlas_lumi'], mode='markers', marker=marker, name='CMS lumi / ATLAS lumi', customdata=customdata, hovertemplate=hovertemplate))
    fig.add_trace(plotly.graph_objects.Scattergl(x=data.UTC, y=data['atlas_beta_star (cm)'], mode='markers', name='β* (IP1) (cm)', yaxis='y2'))
    fig.add_trace(plotly.graph_objects.Scattergl(x=data.UTC, y=data['cms_crossing_angle (urad)'], mode='markers', name='crossing angle (V) (IP1) (urad)', yaxis='y3'))
    fig.update_layout(
        template='plotly_dark',
        hovermode='x',
        font=dict(size=18),
        # xaxis=dict(domain=[0.1, 1]),
        yaxis = dict(range=[0.96, 1.04], side='right'),
        yaxis2 = dict(tickfont=dict(color=colorway[1]), range=[25, 125], overlaying='y'),
        yaxis3 = dict(tickfont=dict(color=colorway[2]), range=[120, 180], anchor='free', overlaying='y', autoshift=True)
        )
    fig.write_html(f"/eos/home-a/adelanno/www/tmp/{data.attrs['fill_number']}.html")

def histogram(data: pandas.DataFrame, after: str = None):
    fill_number = data.attrs['fill_number']
    data = data.set_index('UTC')
    if after:
        data = data[after:]
    # data = data[data['cms_lumi/atlas_lumi'] < 2]
    xlim = (0.97, 1)
    ratio = data['cms_lumi/atlas_lumi'][(data['cms_lumi/atlas_lumi'] > xlim[0]) & (data['cms_lumi/atlas_lumi'] < xlim[1])]
    hist = plotly.graph_objects.Histogram(x=ratio, name='cms_lumi/atlas_lumi', autobinx=True)
    fig = plotly.graph_objects.Figure(data=[hist])
    t0, t1 = data.index[0].strftime('%Y-%m-%dT%H:%M:%SZ'), data.index[-1].strftime('%Y-%m-%dT%H:%M:%SZ')
    fig.update_layout(title_text=f"cms_lumi/atlas_lumi [fill {fill_number}] [{t0} - {t1}]", xaxis_title_text='cms_lumi/atlas_lumi', font=dict(size=24))
    fig.write_html(f'/eos/home-a/adelanno/www/tmp/{fill_number}-hist.html')

def get_fills(year: int = 2024, min_date: str = '2024-01-01', max_date: str = '2024-12-31', min_duration: str = '6 hour', max_duration: str = '48 hour'):
    import query
    fills = pandas.DataFrame(query.DB.get_lhc_fills_by_time(from_time=min_date, to_time=max_date, beam_modes='STABLE'))
    stable = pandas.json_normalize(fills.beamModes.apply(lambda row: [_ for _ in row if (_['mode'] == 'STABLE')][0]))[['startTime', 'endTime']].apply(pandas.to_datetime, unit='s', utc=True)
    fills['fillNumber'], fills['startTime'], fills['endTime'], fills['duration'] = fills.fillNumber, stable.startTime, stable.endTime, (stable.endTime - stable.startTime)
    date_filter = (fills.startTime > min_date) & (fills.endTime < max_date)
    duration_filter = (fills.duration >= min_duration) & (fills.duration <= max_duration)
    return fills[date_filter & duration_filter].fillNumber.astype(int).to_list()
    # fills = pandas.json_normalize(pandas.read_json(f'https://lpc.web.cern.ch/cgi-bin/fillTableReader.py?action=load&year={year}').data)
    # date_filter = (pandas.to_datetime(fills.start_sb) > min_date) & (pandas.to_datetime(fills.start_sb) < max_date)
    # duration_filter = (pandas.to_timedelta(fills.length_sb) >= min_duration) & (pandas.to_timedelta(fills.length_sb) <= max_duration)
    # return fills[date_filter & duration_filter].fillno.astype(int).to_list()

def get_head_on_fills(min_date: str):
    fills = get_fills(min_date=min_date, min_duration='6 hour')
    # _ = [lumi_data(fill).to_pickle(f'/eos/home-a/adelanno/www/tmp/{fill}.pkl') for fill in fills]
    data = {fill: pandas.read_pickle(f'/eos/home-a/adelanno/www/tmp/{fill}.pkl') for fill in fills}
    return {fill: data[fill][(data[fill]['atlas_beta_star (cm)'] == 30) & (data[fill].atlas_lumi_leveling_type == 'NONE') & (data[fill].cms_lumi_leveling_type == 'NONE')] for fill in fills}

def stacked_histogram(min_fill: int, xlim: tuple[float, float] = (0.96, 1.0)):
    head_on_data = get_head_on_fills(min_date='2024-07-01')
    fills = [fill for fill in head_on_data.keys() if fill >= min_fill]
    print(fills)
    fig = plotly.graph_objects.Figure()
    for fill in fills:
        ratio = head_on_data[fill]['cms_lumi/atlas_lumi']
        ratio = ratio[(ratio >= xlim[0]) & (ratio <= xlim[1])]
        fig.add_trace(plotly.graph_objects.Histogram(x=ratio, name=str(fill), autobinx=True))
    fig.update_layout(barmode='stack', title_text=f'cms_lumi/atlas_lumi', xaxis_title_text='cms_lumi/atlas_lumi', font=dict(size=24))
    fig.write_html(f'/eos/home-a/adelanno/www/tmp/{min(fills)}-{max(fills)}-hist.html')

def main(fill: int):
    data = lumi_data(fill)
    if ((data['atlas_beta_star (cm)'] == 30) & (data.atlas_lumi_leveling_type == 'NONE') & (data.cms_lumi_leveling_type == 'NONE')).any():
        print(f'{fill} reached head-on period; plotting...')
        ratio(data)
        histogram(data=data[(data['atlas_beta_star (cm)'] == 30) & (data.atlas_lumi_leveling_type == 'NONE') & (data.cms_lumi_leveling_type == 'NONE')])

def hist_9877():
    data = lumi_data(9877).set_index('UTC')['2024-07-08 21:30:00':]
    data = data[data['cms_lumi/atlas_lumi'] < 2]

def multiple_ratios():
    import matplotlib
    lumi = {int(file.stem): pandas.read_pickle(file) for file in pathlib.Path('.').glob('98*.pkl')} 
    ax = lumi[9840].reset_index().plot(label='9840', kind='scatter', x='index', y='cms_lumi/atlas_lumi', s=1, color='red', alpha=0.5, ylim=(0.95, 1.05))
    ax = lumi[9842].reset_index().plot(label='9842', kind='scatter', x='index', y='cms_lumi/atlas_lumi', s=1, color='blue', alpha=0.5, ax=ax)
    ax = lumi[9849].reset_index().plot(label='9849', kind='scatter', x='index', y='cms_lumi/atlas_lumi', s=1, color='green', alpha=0.5, ax=ax)
    ax = lumi[9852].reset_index().plot(label='9852', kind='scatter', x='index', y='cms_lumi/atlas_lumi', s=1, color='black', alpha=0.5, ax=ax)
    ax = lumi[9860].reset_index().plot(label='9860', kind='scatter', x='index', y='cms_lumi/atlas_lumi', s=1, color='magenta', alpha=0.5, ax=ax)
    matplotlib.pyplot.show()
