#!/usr/bin/env bash

# https://acrondocs.web.cern.ch/

[[ "${HOSTNAME}" == 'lxplus'*'cern.ch' ]] || return 1

repo_location="${HOME}/lumi_stuff/timber" # https://gitlab.cern.ch/adelanno/lumi_stuff/-/tree/master/timber
query_command="/home/acc-py/venvs/pytimber/bin/python3.7 ${repo_location}/automaticScan.py"
acron jobs create --schedule '0 * * * *' --target 'brilsrv01.cern.ch' --command "${query_command}" --description 'timber autoscan query'

eos_dir="/eos/home-${USER:0:1}/${USER}/www/vdm/autoscan/"
mkdir -p "${eos_dir}"
transfer_command="find ${repo_location}/data/ -type f -size +1k | xargs -I_ xrdcp --force --silent _ ${eos_dir}"
acron jobs create --schedule '5 * * * *' --target 'lxplus.cern.ch' --command "${transfer_command}" --description 'timber autoscan xrdcp eos'
