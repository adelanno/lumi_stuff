#!/home/acc-py/venvs/pytimber/bin/python

from __future__ import annotations
import logging
import pathlib
import sys

import numpy
import pandas
import pytimber

''' https://nxcals-docs.web.cern.ch/current/user-guide/data-access/nxcals-access-request/ '''

logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')

DB = pytimber.LoggingDB(source='nxcals') # https://nxcals-docs.web.cern.ch/current/python-nxcals-docs/api/pytimber.LoggingDB.html

def fill_timestamps(fill_number: int, beam_modes: list[str] = ['ADJUST', 'STABLE']) -> tuple[str, str]:
    '''Query start and end timestamps between `beam_modes` during `fill`.'''
    logging.info(f'querying timestamps for fill {fill_number}')
    # fill_beam_modes = pandas.DataFrame(DB.getLHCFillData(fill_number=fill_number)['beamModes'])
    fill_beam_modes = pandas.DataFrame(DB.get_lhc_fill_data(fill_number=fill_number)['beamModes'])
    ts = fill_beam_modes[fill_beam_modes['mode'].isin(beam_modes)]
    return ts.startTime.min(), ts.endTime.max() # pandas.to_datetime(arg=(ts.startTime.min(), ts.endTime.max()), unit='s', utc=True).astype(str)

def ts_to_unix(t1: str|int|float, t2: str|int|float, tz: str = 'utc') -> int|float:
    '''Convert `t1` & `t2` to unix timestamps.'''
    if isinstance(t1, str) and isinstance(t2, str):
        t1 = pandas.Timestamp(t1, tz=tz).timestamp() # pytimber.parsedate(t1, zone=tz)
        t2 = pandas.Timestamp(t2, tz=tz).timestamp() # pytimber.parsedate(t2, zone=tz)
    assert t1 > pandas.Timestamp('2000-01-01').timestamp()
    assert t1 < t2
    return (t1, t2)

def query(pattern_or_list: str | list[str], t1: str, t2: str, unix_time=True) -> dict[str, tuple[numpy.array, numpy.array]]:
    '''https://nxcals-docs.web.cern.ch/current/python-nxcals-docs/api/pytimber.LoggingDB.get.html'''
    logging.info(f'querying from {t1} to {t2}')
    t1, t2 = ts_to_unix(t1=t1, t2=t2)
    return DB.get(pattern_or_list=pattern_or_list, t1=t1, t2=t2, unixtime=unix_time)

def array_size(response: tuple[str, tuple[numpy.array, numpy.array]]) -> int:
    '''Return size of timestamp array in `response`.'''
    (key, (ts, data)) = response
    assert ts.size == numpy.array(data).shape[0]
    return ts.size

def to_dataframe(response: tuple[numpy.array, numpy.array], label: str) -> pandas.DataFrame:
    '''Wrap `response` in a `pandas.DataFrame`.'''
    (ts, data) = response
    return pandas.DataFrame({'UTC': pandas.to_datetime(ts, unit='s', utc=True), label: list(data)})

def merge_asof(response: dict[str, tuple[numpy.array, numpy.array]], variables: dict[str, str]):
    '''Merge multiple responses (sorted by granularity of their timestamps) on closest prior timestamp.'''
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.merge_asof.html
    response = dict(sorted(response.items(), key=array_size, reverse=True))
    data = pandas.DataFrame()
    for var, resp in response.items():
        data = to_dataframe(response=resp, label=variables[var]) if data.empty else pandas.merge_asof(data, to_dataframe(response=resp, label=variables[var]), direction='backward')
    return data

def test():
    t1, t2 = fill_timestamps(fill_number=9510)
    variables = {
            'HX:BETASTAR_IP1': 'atlas_beta_star',
            # 'HX:BETASTAR_IP5': 'cms_beta_star',
            'ATLAS:LUMI_TOT_INST': 'atlas_lumi',
            'CMS:LUMI_TOT_INST': 'cms_lumi',
            # 'ATLAS:BUNCH_LUMI_INST': 'atlas_bunch_lumi',
            # 'CMS:BUNCH_LUMI_INST': 'cms_bunch_lumi',
            'UCAP.LHC.IP_POS.IP1:IpPositionVsLsaRef:verIpHalfCrossingAngle': 'atlas_crossing_angle',
            'UCAP.LHC.IP_POS.IP5:IpPositionVsLsaRef:horIpHalfCrossingAngle': 'cms_crossing_angle',
            }
    response = query(pattern_or_list=list(variables.keys()), t1=t1, t2=t2)
    data = merge_asof(response=response, variables=variables)
    return data


class Trash:

    def fill_timestamps(fill_number: int, beam_modes: list[str] = ['ADJUST', 'STABLE']) -> tuple[str, str]:
        beam_modes = DB.getLHCFillData(fill_number=fill_number)['beamModes']
        ts = pandas.to_datetime([ts for _ in [(mode['startTime'], mode['endTime']) for mode in beam_modes if mode['mode'] in modes] for ts in _], unit='s', utc=True)
        return str(ts.min()), str(ts.max())

    def fills(year: int = pandas.Timestamp.now().year) -> pandas.DataFrame:
        '''https://lpc.web.cern.ch/annotatedFillTable.html'''
        url = f'https://lpc.web.cern.ch/cgi-bin/fillTableReader.py?action=load&year={year}'
        logging.info(url)
        data = pandas.json_normalize(pandas.read_json(url).get('data'))
        data = data.replace('<br>', '', regex=True).apply(pandas.to_numeric, errors='ignore')
        data['start_sb'], data['length_sb'] = pandas.to_datetime(data.start_sb).dt.tz_localize('Europe/Zurich').dt.tz_convert('UTC'), pandas.to_timedelta(data.length_sb)
        return data.rename(columns=dict(fillno='fill_number', ta='turn_around_time_statistics', fl='fill_length_statistics', hasscheme='has_scheme'))

    @classmethod
    def latest_fill(cls):
        fill_data = cls.fills()
        fill = fill_data.iloc[fill_data[fill_data.type == 'physics'].start_sb.idxmax()]
        t1, t2 = fill.start_sb.strftime('%Y-%m-%d %H:%M:%S'), (fill.start_sb + fill.length_sb).strftime('%Y-%m-%d %H:%M:%S')
        lumi = lumi_ratio(t1=t1, t2=t2)
        plot(lumi=lumi, title=fill.fill_number)
