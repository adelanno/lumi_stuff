#!/home/acc-py/venvs/pytimber/bin/python

from __future__ import annotations
import getpass
import logging
import pathlib
import socket

import pandas
import pytimber

logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')

'''
https://nxcals-docs.web.cern.ch/current/user-guide/data-access/nxcals-access-request/
https://e-groups.cern.ch/e-groups/Egroup.do?egroupName=it-hadoop-nxcals-pro-analytics
'''

DB = pytimber.LoggingDB(source='nxcals') # https://nxcals-docs.web.cern.ch/current/python-nxcals-docs/api/pytimber.LoggingDB.html

if pathlib.Path(f'/eos/home-{getpass.getuser()[0]}/{getpass.getuser()}').is_dir():
    OUTPUT_DIR = pathlib.Path(f'/eos/home-{getpass.getuser()[0]}/{getpass.getuser()}/www/vdm/bunch_lengths')
elif socket.gethostname() == 'brilsrv01':
    OUTPUT_DIR = pathlib.Path('/localdata/tmp/bunch_lengths')

VDM_FILL = {
        2022: 8381, # https://cmsoms.cern.ch/cms/fills/report?cms_fill=8381
        2023: 8999, # https://cmsoms.cern.ch/cms/fills/report?cms_fill=8999
        2024: 9639, # https://cmsoms.cern.ch/cms/fills/report?cms_fill=9639
        }

'''
The logging variables of the BQM measured bunch length are:
LHC.BQM.B1:BUNCH_LENGTHS
LHC.BQM.B2:BUNCH_LENGTHS
These are 4-sigma gaussian equivalents, based off a FWHM measurement.
The arrays contain the data in the first N entries, where N is the number of bunches in the machine. All further entries are 0. The corresponding RF bucket indices can be found in:
LHC.BQM.B1:FILLED_BUCKETS
LHC.BQM.B2:FILLED_BUCKETS
These are 2.5ns RF bucket numbers, with the first one being bucket "1".
To convert to 25ns BI slots / BCID, you will have to do slot = (bucket-1)*10 + 1.
The BQM also logs full-width measurements at 3 cuts at 25%, 50%, and 75% of the peak:
LHC.BQM.B1:BUNCH_LENGTHS_CUT1
LHC.BQM.B1:BUNCH_LENGTHS_CUT2
LHC.BQM.B1:BUNCH_LENGTHS_CUT3
LHC.BQM.B2:BUNCH_LENGTHS_CUT1
LHC.BQM.B2:BUNCH_LENGTHS_CUT2
LHC.BQM.B2:BUNCH_LENGTHS_CUT3
'''

VARS = (
    'LHC.BQM.B1:BUNCH_LENGTHS', 'LHC.BQM.B2:BUNCH_LENGTHS',
    'LHC.BQM.B1:FILLED_BUCKETS', 'LHC.BQM.B2:FILLED_BUCKETS',
    'LHC.BQM.B1:BUNCH_LENGTHS_CUT1', 'LHC.BQM.B1:BUNCH_LENGTHS_CUT2', 'LHC.BQM.B1:BUNCH_LENGTHS_CUT3',
    'LHC.BQM.B2:BUNCH_LENGTHS_CUT1', 'LHC.BQM.B2:BUNCH_LENGTHS_CUT2', 'LHC.BQM.B2:BUNCH_LENGTHS_CUT3',
    )

def fill_timestamps(fill_number: int, beam_modes: list[str] = ['STABLE']) -> tuple[pandas.Timestamp, pandas.Timestamp]:
    '''Query start and end timestamps for `beam_modes` during `fill`.'''
    logging.info(f'querying timestamps for fill {fill_number}')
    try: 
        fill_beam_modes = DB.get_lhc_fill_data(fill_number=fill_number)
    except AttributeError:
        fill_beam_modes = DB.getLHCFillData(fill_number=fill_number)
    fill_beam_modes = pandas.DataFrame(fill_beam_modes['beamModes']) 
    ts = fill_beam_modes[fill_beam_modes['mode'].isin(beam_modes)]
    return pandas.to_datetime(arg=(ts.startTime.min(), ts.endTime.max()), unit='s', utc=True)

def export(variables: str|list[str], start: pandas.Timestamp, end: pandas.Timestamp, hourly_interval: int = 1):
    '''Export `variables` for the time range between `start` and `end` in `interval` intervals.'''
    year = start.year
    start_range = pandas.date_range(start=start, end=end, freq=f'{hourly_interval}h', tz='utc')
    end_range = start_range + pandas.Timedelta(hours=hourly_interval)
    (OUTPUT_DIR/f'{year}').mkdir(parents=True, exist_ok=True)
    for t1, t2 in zip(start_range, end_range):
        output_file = OUTPUT_DIR/f'{year}'/f"{t1.strftime('%Y%m%d_%H%M%S')}.pkl"
        if output_file.exists():
            logging.warning(f'{output_file} already exists... skipping query')
            continue
        logging.info(f"\nquerying '{t1}' -- '{t2}'")
        data = DB.get(pattern_or_list=variables, t1=t1, t2=t2)
        pandas.Series(data).to_pickle(output_file)

def main():
    year = 2024
    start, end = fill_timestamps(fill_number=VDM_FILL[year])
    export(variables=VARS, start=start, end=end, hourly_interval=1)
