#!/home/acc-py/venvs/pytimber/bin/python

from __future__ import annotations
import logging
import pathlib
import sys

# import matplotlib.dates
# import matplotlib.pyplot
import numpy
import pandas
import pytimber

import query

''' https://nxcals-docs.web.cern.ch/current/user-guide/data-access/nxcals-access-request/ '''

VARIABLES = {
    'ATLAS:LUMI_TOT_INST': 'atlas_lumi (Hz/ub)',
    'HX:BETASTAR_IP1': 'atlas_beta_star (cm)',
    'UCAP.LHC.IP_POS.IP1:IpPositionVsLsaRef:verIpHalfCrossingAngle': 'atlas_crossing_angle (urad)',
    'UCAP.LHC.IP_POS.IP1:IpPositionVsLsaRef:horIpSeparation': 'atlas_horizontal_separation', # UCAP.LHC.IP_POS.IP1:IpPositionVsLsaRef:horRefIpSeparation
    'UCAP.LHC.IP_POS.IP1:IpPositionVsLsaRef:verIpSeparation': 'atlas_vertical_separation', # UCAP.LHC.IP_POS.IP1:IpPositionVsLsaRef:verRefIpSeparation
    'CMS:LUMI_TOT_INST': 'cms_lumi (Hz/ub)',
    'HX:BETASTAR_IP5': 'cms_beta_star (cm)',
    'UCAP.LHC.IP_POS.IP5:IpPositionVsLsaRef:horIpHalfCrossingAngle': 'cms_crossing_angle (urad)',
    'UCAP.LHC.IP_POS.IP5:IpPositionVsLsaRef:horIpSeparation': 'cms_horizontal_separation', # UCAP.LHC.IP_POS.IP5:IpPositionVsLsaRef:horRefIpSeparation
    'UCAP.LHC.IP_POS.IP5:IpPositionVsLsaRef:verIpSeparation': 'cms_vertical_separation', # UCAP.LHC.IP_POS.IP5:IpPositionVsLsaRef:verRefIpSeparation
    'UCAP.ATLAS.LUMI:LevellingState:levellingType': 'atlas_lumi_leveling_type',
    'UCAP.CMS.LUMI:LevellingState:levellingType': 'cms_lumi_leveling_type',
    # 'LHC.LUMISERVER:LumiLevelingIP1:LevelStep': 'atlas_lumi_leveling_active',
    # 'LHC.LUMISERVER:LumiLevelingIP5:LevelStep': 'cms_lumi_leveling_active',
    }

def atlas_massi_files():
    return query.DB.getDescription('%ATLAS.OFFLINE%')

def query_variables(t1: str, t2: str):
    response = query.query(pattern_or_list=list(VARIABLES.keys()), t1=t1, t2=t2)
    return query.merge_asof(response=response, variables=VARIABLES)

def colorbar(ax: matplotlib.pyplot.Axes, series: pandas.Series):
    fig = ax.get_figure()
    norm = matplotlib.colors.Normalize(vmin=series.min(), vmax=series.max())
    fig.colorbar(mappable=matplotlib.cm.ScalarMappable(norm=norm, cmap='viridis'), location='bottom', label=series.name, aspect=60)

def secondary_y(ax: matplotlib.pyplot.Axes, data: pandas.DataFrame, column: str, color: str, position: float, ylim: tuple[float, float]):
    _ax = ax.twinx() # https://github.com/pandas-dev/pandas/issues/9909
    data.plot(kind='scatter', x='UTC', y=column, c=color, alpha=0.005, ylim=ylim, ax=_ax)
    _ax.yaxis.label.set_color(color)
    _ax.tick_params(axis='y', color=color, labelcolor=color)
    _ax.spines.right.set_position(('axes', position))
    return _ax

def plot(data: pandas.DataFrame, title: str, ylim: tuple[float, float] = (0.95, 1.1)):
    logging.info('plotting...')
    ax = data.plot(kind='scatter', x='UTC', y='cms_lumi/atlas_lumi', c='SBIL', colormap='viridis', colorbar=False, s=4, ylim=ylim)
    colorbar(ax=ax, series=data['SBIL'])
    _ax = secondary_y(ax=ax, data=data, column='atlas_beta_star (cm)', color='lightgray', position=1.0, ylim=(30, 60))
    _ax = secondary_y(ax=ax, data=data, column='atlas_crossing_angle (urad)', color='skyblue', position=1.1, ylim=(120,170))
    # _ax = secondary_y(ax=ax, data=data, column='cms_crossing_angle (urad)', color='lightcoral', position=1.2, ylim=(150,170))
    ax.set_title(title)
    ax.xaxis.set_major_formatter(matplotlib.dates.ConciseDateFormatter(ax.xaxis.get_major_locator())) # https://matplotlib.org/stable/gallery/ticks/date_concise_formatter.html
    matplotlib.pyplot.tight_layout()
    matplotlib.pyplot.savefig(fname=f'{title}.png', dpi=800)
    matplotlib.pyplot.show()

def plt_bkgd():
    plt_bkgd = query.DB.search('DIP.CMS:BackgroundSources:PLT%')
    data = dict()
    for fill_number in fills:
        t1, t2 = query.fill_timestamps(fill_number=fill_number, beam_modes=['ADJUST', 'STABLE'])
        data[fill] = data(t1=t1, t2=t2)
        return 
    pattern_or_list = ['CMS:LUMI_TOT_INST', *plt_bkgd] # 'DIP.CMS:BackgroundSources:PLT_LEAD_1', 'DIP.CMS:BackgroundSources:PLT_LEAD_2', 'DIP.CMS:BackgroundSources:PLT_NC_1', 'DIP.CMS:BackgroundSources:PLT_NC_2']
    response = query.query(pattern_or_list=pattern_or_list, t1='2024-04-14 17:15:00', t2='2024-04-14 23:00:00')
    data = query.merge_asof(response=response, variables=dict(zip(pattern_or_list, [pat.split(':')[-1] for pat in pattern_or_list])))
    logging.info('plotting...')
    import plotly.graph_objects
    import plotly.subplots
    # https://plotly.com/python/multiple-axes/#multiple-axes
    fig = plotly.graph_objects.Figure()
    fig.add_trace(plotly.graph_objects.Scatter(x=data.UTC, y=data.LUMI_TOT_INST, name='LUMI_TOT_INST'))
    fig.add_trace(plotly.graph_objects.Scatter(x=data.UTC, y=data.PLT_LEAD_1, name='plt_lead_1', yaxis='y1'))
    fig.add_trace(plotly.graph_objects.Scatter(x=data.UTC, y=data.PLT_NC_1, name='plt_nc_1', yaxis='y2'))
    # fig.update_layout(
    #         )

    fig = plotly.subplots.make_subplots(specs=[[{"secondary_y": True}]])
    fig.add_trace(plotly.graph_objects.Scatter(x=data.UTC, y=data.LUMI_TOT_INST), secondary_y=False)
    fig.add_trace(plotly.graph_objects.Scatter(x=data.UTC, y=data.PLT_LEAD_1), secondary_y=True)
    fig.add_trace(plotly.graph_objects.Scatter(x=data.UTC, y=data.PLT_NC_1), secondary_y=True)

def multiple_ratios(fills: list[int]):
    LUMI = {fill_number: query_variables(*query.fill_timestamps(fill_number, ['ADJUST', 'STABLE'])) for fill_number in fills}
    for fill, df in LUMI.items():
        df['cms_lumi/atlas_lumi'] = df['cms_lumi (Hz/ub)'] / df['atlas_lumi (Hz/ub)']
        df['SBIL'] = df['cms_lumi (Hz/ub)'] / 2340
        df.to_pickle(f'{fill}.pkl')
    #     ax = df.plot(kind='scatter', x='UTC', y='cms_lumi/atlas_lumi', c='SBIL', colormap='viridis', colorbar=False, s=4, ylim=(0.8,1.2))
    #     colorbar(ax=ax, series=lumi['SBIL'])
    # ax.xaxis.set_major_formatter(matplotlib.dates.ConciseDateFormatter(ax.xaxis.get_major_locator())) # https://matplotlib.org/stable/gallery/ticks/date_concise_formatter.html
    # matplotlib.pyplot.tight_layout()
    # LUMI = {int(file.stem): pandas.read_pickle(file) for file in pathlib.Path('.').glob('98*.pkl')}

def main():
    # fill_number = 9852
    for fill_number in [9840, 9842, 9849, 9852, 9860]:
        t1, t2 = query.fill_timestamps(fill_number=fill_number, beam_modes=['ADJUST', 'STABLE'])
        lumi = lumi(t1=t1, t2=t2)
        lumi['cms_lumi/atlas_lumi'] = lumi['cms_lumi (Hz/ub)'] / lumi['atlas_lumi (Hz/ub)']
        plot(lumi=lumi, title=fill_number, ylim=(0.95,1.05))

if __name__ == '__main__':
    main()
