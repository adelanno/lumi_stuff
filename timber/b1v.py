#!/home/acc-py/venvs/pytimber/bin/python

import logging

import pandas
import pytimber

logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')

DB = pytimber.LoggingDB(source='nxcals') # https://nxcals-docs.web.cern.ch/current/python-nxcals-docs/api/pytimber.LoggingDB.html

def B1V(t1: str , t2: str, var: str = 'LHC.BSRT.5R4.B1:BUNCH_EMITTANCE_V') -> pandas.Series:
    response = DB.get(pattern_or_list=[var], t1=t1, t2=t2)
    return pandas.DataFrame(response[var][1]).median(axis=0)

def validate_gated():
    b1v = B1V(t1='2024-05-16 16:30:00', t2='2024-05-16 16:35:00')
    decent_bcid = b1v[(b1v>1) & (b1v<3.5)].index
    gated = [302, 323, 344, 505, 526, 821, 1080, 1101, 1396, 1999, 2964, 3122]
    assert pandas.Series(gated).isin(decent_bcid).all() == True

def b1v_evolution():
    import matplotlib.pyplot, query
    t1, t2 = '2024-05-16 16:00:00', '2024-05-17 10:00:00'
    variables = {'B1H': 'LHC.BSRT.5R4.B1:BUNCH_EMITTANCE_H', 'B1V': 'LHC.BSRT.5R4.B1:BUNCH_EMITTANCE_V', 'B2H': 'LHC.BSRT.5L4.B2:BUNCH_EMITTANCE_H', 'B2V': 'LHC.BSRT.5L4.B2:BUNCH_EMITTANCE_V'}
    response = query.query(pattern_or_list=list(variables.values()), t1=t1, t2=t2)
    b1v = pandas.Series(response['LHC.BSRT.5R4.B1:BUNCH_EMITTANCE_V'][1].mean(axis=0))
    b1v[b1v>3.5]
    data = query.merge_asof(response=response, variables=variables)
    pandas.Series(index=data.UTC, data=data.B1V.str[3].to_numpy()).plot(title='B1V BCID:0003')
    matplotlib.pyplot.show()

if __name__ == '__main__':
    validate_gated()
