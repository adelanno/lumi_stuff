#!/home/acc-py/venvs/pytimber/bin/python

from __future__ import annotations
import calendar
import logging
import pathlib

import numpy
import pandas
import pytimber

logging.basicConfig(level=logging.WARNING)

# https://nxcals-docs.web.cern.ch/current/user-guide/data-access/nxcals-access-request/

'''https://nxcals-docs.web.cern.ch/current/python-nxcals-docs/api/pytimber.LoggingDB.html'''
DB = pytimber.LoggingDB(source='nxcals')

WORK_DIR = pathlib.Path(str(pathlib.Path.home()).replace('user', 'work'))
OUTPUT_PATH = WORK_DIR/'www/vdm/autoscan'
OUTPUT_PATH.mkdir(exist_ok=True)

NOW = pandas.Timestamp.now()

def query(pattern_or_list: str | list[str], t1: str, t2: str) -> dict[str, tuple[numpy.ndarray, numpy.ndarray]]:
    try:
        logging.info(f'querying from {t1} - {t2}')
        t1 = pandas.Timestamp(t1, tz='utc').timestamp() # pytimber.parsedate(t1)
        t2 = pandas.Timestamp(t2, tz='utc').timestamp() # pytimber.parsedate(t2)
        return DB.get(pattern_or_list=pattern_or_list, t1=t1, t2=t2)
    except ValueError:
        logging.error('could not parse provided timestamp(s)')

def monthRange(year: int, month: int) -> tuple[str, str]:
    t1 = f'{year}-{month:02}-01 00:00:00'
    t2 = f'{year}-{month:02}-{calendar.monthrange(year, month)[1]} 23:59:59'
    return t1, t2

def autoScans(t1: str = None, t2: str = None, IP: str = 'IP5') -> pandas.DataFrame:
    data = query(pattern_or_list=f'LHC.LUMISERVER:AutomaticScan{IP}:%', t1=t1, t2=t2)
    data = [pandas.Series(index=val[0], data=val[1]).rename(key.split(':')[-1]) for key, val in data.items()]
    data = pandas.concat(data, axis=1, join='outer').rename_axis('timestamp')
    data.insert(loc=0, column='datetime', value=pandas.to_datetime(data.index, unit='s', utc=True))
    return data

def exportMonth(year: int, month: int):
    output_file = OUTPUT_PATH/f'{year}-{month:02}.csv'
    t1, t2 = monthRange(year=year, month=month)
    data = autoScans(t1=t1, t2=t2)
    if data.empty:
        return
    # print(output_file)
    data.to_csv(output_file)

def pastData():
    for year in range(2021, NOW.year):
        for month in range(1, 13):
            exportMonth(year=year, month=month)
    for month in range(1, NOW.month):
        exportMonth(year=NOW.year, month=month)

def loadCSV():
    files = sorted(OUTPUT_PATH.glob('20*.csv'))
    data = [pandas.read_csv(csv, parse_dates=['datetime'], low_memory=False) for csv in files]
    data = pandas.concat(data, axis=0)
    return data

def main():
    exportMonth(year=NOW.year, month=NOW.month)
    # pastData()
    loadCSV().to_csv(OUTPUT_PATH/'vdm_timestamps.csv', index=False)

if __name__ == '__main__':
    main()


class Trash:

    def activeScan(t1: str = None, t2: str = None, IP: str = 'IP5') -> pandas.DataFrame:
        var = f'LHC.LUMISERVER:AutomaticScan{IP}:Active'
        data = query(pattern_or_list=var, t1=t1, t2=t2).get(var)
        if data:
            return pandas.DataFrame({'timestamp': pandas.to_datetime(data[0], unit='s', utc=True), 'scan_active': data[1].astype(bool)})
