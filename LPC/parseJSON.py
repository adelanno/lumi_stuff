#!/cvmfs/cms-bril.cern.ch/brilconda310/bin/ipython3

from __future__ import annotations
import html
import json
import pathlib
import re
import typing

'''https://gitlab.cern.ch/lhc-injection-scheme/injection-schemes'''

def filled_bunches(beam: list[str, typing.Any]) -> set[int]:
    '''Return the BCID of all filled bunches.'''
    filled_bcid = set()
    for group in beam:
        if group['injectionType'].lower() not in ('nominal', 'intermediate'):
            continue
        lhc_bunch = group['lhcbunch']
        for batch in group['batches']:
            number_filled_bunches = batch['bunches']
            bcid_is_filled = batch['bunchArray']
            assert number_filled_bunches == sum(bcid_is_filled)
            start_bcid = lhc_bunch + batch['injBunch']
            bcid = range(start_bcid, start_bcid + len(bcid_is_filled))
            filled_bcid.update({bcid for bcid, is_filled in dict(zip(bcid, bcid_is_filled)).items() if is_filled})
    return filled_bcid

def test():
    if not pathlib.Path('injection-schemes/').exists():
        raise ValueError('Make sure to `git clone` the following repo:\nhttps://gitlab.cern.ch/lhc-injection-scheme/injection-schemes')
    for scheme_file in pathlib.Path('injection-schemes/').glob('*.json'):
        scheme = json.load(scheme_file.open())
        file_name = dict(zip(['filled_beam1', 'colliding_ip1'], map(int, re.search('(\d+)b_(\d+)_', scheme_file.stem).group(1, 2))))
        filled = {'beam1': filled_bunches(beam=scheme['beam1']), 'beam2': filled_bunches(beam=scheme['beam2'])}
        colliding_ip1 = filled['beam1'] & filled['beam2']
        if file_name['filled_beam1'] != len(filled['beam1']):
            print(f"{scheme['schemeName']}\n\t{len(filled['beam1']) = }")
        if file_name['colliding_ip1'] != len(colliding_ip1):
            print(f"{scheme['schemeName']}\n\t{len(colliding_ip1) = }")

try: 

    import awkward

    def ak_filled_bunches(beam: awkward.Array) -> awkward.Array:
        '''Return BCIDs for filled bunches.'''
        bcid = beam.lhcbunch + beam.batches.injBunch + awkward.local_index(beam.batches.bunchArray)
        bcid_isfilled = awkward.values_astype(beam.batches.bunchArray, bool)
        return bcid[bcid_isfilled]

    def ak_test():
        for scheme_file in pathlib.Path('injection-schemes/').glob('*.json'):
            scheme = awkward.from_json(scheme_file)
            file_name = dict(zip(['filled_beam1', 'colliding_ip1'], map(int, re.search('(\d+)b_(\d+)_', scheme_file.stem).group(1, 2))))
            filled = awkward.Record({'beam1': ak_filled_bunches(scheme.beam1), 'beam2': ak_filled_bunches(scheme.beam2)})
            colliding_ip1 = set(awkward.ravel(filled.beam1)) & set(awkward.ravel(filled.beam2))
            if file_name['filled_beam1'] != awkward.count(filled.beam1):
                print(f"{scheme.schemeName}\n\t{awkward.count(filled.beam1) = }")
            if file_name['colliding_ip1'] != len(colliding_ip1):
                print(f"{scheme['schemeName']}\n\t{len(colliding_ip1) = }")

except ImportError:
    pass

def parse_active_filling_scheme(scheme_name: str):
    afs = scheme_name.lower().split('_')
    mode = afs.pop(0) if afs[0] in ('single', 'multi') else None
    bunch_spacing = int(afs.pop(0).rstrip('ns')) if re.match('\d+ns$', afs[0]) else None
    if re.match('\d+b$', afs[0]):
        bunches_per_beam = int(afs.pop(0).rstrip('b'))
        colliding_bunches_ip1 = int(afs.pop(0))
        colliding_bunches_ip2 = int(afs.pop(0))
        colliding_bunches_ip8 = int(afs.pop(0))
    bunches_per_injection = {'_': int(_.rstrip('bpi')) for _ in afs if re.match('\d+bpi$', _)}.pop('_', None)
    injections_per_bunch = {'_': int(_.rstrip('inj')) for _ in afs if re.match('\d+inj$', _)}.pop('_', None)
    individual_bunches = {'_': int(_.rstrip('indivs')) for _ in afs if re.match('\d+indivs?$', _)}.pop('_', None)
    non_colliding_bunches = {'_': int(_.rstrip('ncol')) for _ in afs if re.match('\d+nco?l?$', _)}.pop('_', None)
    long_range = {'_': _.rstrip('lr') for _ in afs if re.match('\w+lr$', _)}.pop('_', None)
    ions = any('pbpb' in _ for _ in afs)
    ppref = any('ppref' in _ for _ in afs)
    vdm = any('vdm' in _ for _ in afs)
    hybrid = ('hybrid' in afs)
    mixed = ('mixed' in afs)
    high_beta = ('highbeta' in afs)
    return dict(scheme=scheme_name.lower(), mode=mode, bunch_spacing=bunch_spacing, bunches_per_beam=bunches_per_beam, colliding_bunches_ip1=colliding_bunches_ip1, colliding_bunches_ip2=colliding_bunches_ip2,
        colliding_bunches_ip8=colliding_bunches_ip8, bunches_per_injection=bunches_per_injection, injections_per_bunch=injections_per_bunch, individual_bunches=individual_bunches,
        non_colliding_bunches=non_colliding_bunches, long_range=long_range, ions=ions, ppref=ppref, vdm=vdm, hybrid=hybrid, mixed=mixed, high_beta=high_beta)

if __name__ == '__main__':
    test()
    try:
        ak_test()
        lpc_test()
    except NameError:
        pass
