#!/usr/bin/env python3

from __future__ import annotations
import itertools
import urllib.request

import pandas

def getFillScheme(url: str) -> list[str]:
    return urllib.request.urlopen(url).read().decode('utf-8').splitlines()

def linesBetweenDelimiters(lines: list[str], begin: str, end: str) -> list[int]:
    # [How to grep lines between two patterns in a big file with python](https://stackoverflow.com/a/11156534)
    return list(itertools.takewhile(lambda line: end not in line, itertools.dropwhile(lambda line: begin not in line, lines)))

def B1(lines: list[int]) -> pandas.DataFrame:
    b1 = [line.strip().split(',') for line in linesBetweenDelimiters(lines=lines, begin = 'HEAD ON COLLISIONS FOR B1', end = 'HEAD ON COLLISIONS FOR B2')]
    return pandas.DataFrame(data=b1[2:-1], columns=b1[1])

def B2(lines: list[int]) -> pandas.DataFrame:
    b2 = [line.strip().split(',') for line in linesBetweenDelimiters(lines=lines, begin = 'HEAD ON COLLISIONS FOR B2', end='.*')]
    return pandas.DataFrame(data=b2[2:], columns=b2[1])

def collidingBCID(fill_scheme_csv: list[str], IP: str = 'IP5') -> pandas.Series:
    b1 = B1(fill_scheme_csv)
    b2 = B2(fill_scheme_csv)
    return pandas.Series(int(bx)//10 for bx in (set(b1[IP]) & set(b2[IP])) if bx != '-').sort_values().reset_index(drop=True)

def parseFillScheme(fill_scheme: str, year: int = 2023):
    url = f'https://lpc.web.cern.ch/fillingSchemes/{year}/candidates/{fill_scheme}.csv'
    csv = urllib.request.urlopen(url).read().decode('utf-8').splitlines()
    colliding_bcid = collidingBCID(csv)
    groups = colliding_bcid.groupby((colliding_bcid.diff() != 1).cumsum())
    grouped_bcids = [group for index, group in groups]
    indiv = [group.item() for group in grouped_bcids if len(group)==1]
    trains = [group for group in grouped_bcids if len(group)>1]
    leading = [train.iloc[0] for train in trains]
    print(f'\n{fill_scheme}:\n  individual colliding bunches: {indiv}\n  leading train bunches: {leading}')

def stickland202311():
    fill_schemes = ['25ns_399b_386_204_258_128bpi_hybrid_3INDIV',
            '25ns_911b_898_614_688_128bpi_11inj_hybrid_3INDIV',
            '25ns_1178b_1165_696_693_236bpi_9inj_hybrid_2INDIV',
            '25ns_1886b_1873_1217_1173_236bpi_12inj_hybrid_2INDIV',
            '25ns_1903b_1890_1099_1160_236bpi_12inj_hybrid_3INDIV']
    for fill_scheme in fill_schemes:
        parseFillScheme(fill_scheme)


def groupTrains(colliding_bcid: pandas.Series) -> list[tuple[int]]:
    import awkward
    groups = colliding_bcid.groupby((colliding_bcid.diff() != 1).cumsum())
    return awkward.Array(group for index, group in groups)

def leadingCollidingBCID(trains: awkward.Array) -> list[int]:
    return trains[:, 0].to_list()

def trailingCollidingBCID(trains: awkward.Array) -> list[int]:
    return trains[:, -1].to_list()

def bcid_in_middle_of_train(fill_scheme: str = '25ns_75b_62_32_62_12bpi_9inj', n: int = 5):
    url = f'https://lpc.web.cern.ch/fillingSchemes/2023/candidates/{fill_scheme}.csv'
    fill_scheme_csv = getFillScheme(url)
    colliding_bcid = collidingBCID(fill_scheme_csv)
    groups = colliding_bcid.groupby((colliding_bcid.diff() != 1).cumsum())
    trains = [list(group) for index, group in groups]
    return [train[n:-n] for train in trains if len(train) >= 2*n]

def fillSchemes(year: int) -> list[str]:
    url = f'https://lpc.web.cern.ch/cgi-bin/fillTable.py?year={year}'
    response = urllib.request.urlopen(url).read().decode('utf-8')

def test(year: int = 2022):
    fill_table = pandas.read_html('https://lpc.web.cern.ch/cgi-bin/fillTable.py')[1]
    fill_schemes = fill_table[year]['Filling Scheme'].unique().tolist()
    fill_schemes.remove('Single_3b_2_2_2')
    for fill_scheme in fill_schemes:
        try:
            url = f'https://lpc.web.cern.ch/fillingSchemes/{year}/candidates/{fill_scheme}.csv'
            print(url)
            fill_scheme_csv = getFillScheme(url)
            assert len(collidingBCID(fill_scheme_csv)) == int(url.split('_')[2])
        except urllib.request.HTTPError:
            pass
