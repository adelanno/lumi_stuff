#!/cvmfs/cms-bril.cern.ch/brilconda3/bin/python3

import logging

import pandas

'''https://lpc.web.cern.ch/annotatedFillTable.html'''

def fills(year: int) -> pandas.DataFrame:
    url = f'https://lpc.web.cern.ch/cgi-bin/fillTableReader.py?action=load&year={year}'
    logging.debug(url)
    data = pandas.read_json(url).get('data')
    data = pandas.json_normalize(data)
    return data.rename(columns={'ta': 'turn-around-time_statistics', 'fl': 'fill-length_statistics'})
